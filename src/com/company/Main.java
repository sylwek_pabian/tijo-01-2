package com.company;

abstract class Shape {
    // pole powierzchni figury
    public abstract float getArea();
    // obwod figury
    public abstract float getCircuit();
}

// Prosze zaimplementowac klasy Rectangle oraz Circle.
// Klasy powinny dziedziczyc po klasie abstrakcyjnej Shape.
// Na podstawie analizy klasy Main prosze zaimplementowac
// odpowiednie konstruktory dla nowych klas.
// Obie klasy powinny miec wlasna implementacje
// metod getArea() oraz getCircuit().
// Prosze nie modyfikowac istniejacych klas!

class Main {
    public static void main(String[] args) {
        final float CIRCLE_R = 3.0F;
        final float RECTANGLE_WIDTH = 3.0F;
        final float RECTANGLE_HEIGHT = 4.0F;

        final Shape rectangle = new Rectangle(RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
        System.out.println("Pole powierzchni prostokata: " + rectangle.getArea());
        System.out.println("Obwod prostokata: " + rectangle.getCircuit());

        final Shape circle = new Circle(CIRCLE_R);
        System.out.println("Pole powierzchni kola: " + circle.getArea());
        System.out.println("Obwod kola: " + circle.getCircuit());
    }
}