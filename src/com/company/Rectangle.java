package com.company;

public class Rectangle extends Shape {

    float RECTANGLE_WIDTH;
    float RECTANGLE_HEIGHT;

    public Rectangle(float RECTANGLE_WIDTH, float RECTANGLE_HEIGHT){
        this.RECTANGLE_HEIGHT = RECTANGLE_HEIGHT;
        this.RECTANGLE_WIDTH = RECTANGLE_WIDTH;
    }

    @Override
    public float getArea() {
        return RECTANGLE_HEIGHT * RECTANGLE_WIDTH;
    }

    @Override
    public float getCircuit() {
        return (2 * RECTANGLE_WIDTH) + (2 * RECTANGLE_HEIGHT);
    }
}
