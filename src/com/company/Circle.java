package com.company;

public class Circle extends Shape {

    float CIRCLE_R;

    public Circle(float CIRCLE_R){
        this.CIRCLE_R = CIRCLE_R;
    }

    @Override
    public float getArea() {
        return (float) (CIRCLE_R * CIRCLE_R * Math.PI);
    }

    @Override
    public float getCircuit() {
        return (float) (2 * CIRCLE_R * Math.PI);
    }
}